# select parent image
FROM maven:3.6.3-jdk-8

ARG JAR_FILE=target/*.jar

WORKDIR /app
# copy the source tree and the pom.xml to our new container
COPY src /app/src

COPY pom.xml /app

# package our application code
RUN mvn clean package

RUN mv ${JAR_FILE} demo.jar

# set the startup command to execute the jar
CMD ["java", "-jar", "demo.jar"]
